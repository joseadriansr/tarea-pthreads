#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>                       // time()
#include <pthread.h>
#include "../include/pi.h"
#include "../include/mult_vctr_mtrx.h"

typedef struct                          // struct para guardar el resultado de pi con pthreads 
{
    double aprox_pi_pthreads; 
} RESULTADO;

#define NUM_THREADS 5                   // numero de threads
#define NUM_TERMINOS_PI 1000            // numero de terminos para la serie de leibniz
#define upper 8                         // limite superior para rand() n de tamano para la matriz y el vector
#define lower 3                         // limite inferior para rand() n de tamano para la matriz y el vector

RESULTADO pthreads_resultado;           // objeto del struct resultados
pthread_t threads[NUM_THREADS];         // threads para pi
pthread_t threads_mult[8];              // threads para mult_vctr_mtrx
pthread_mutex_t mutex;                  // variable de sincronizacion mutex
pthread_mutex_t mutex_mtrx;                  // variable de sincronizacion mutex

double pth_pi_aprox(int n);             // declaracion de las funciones necesarias para pi con pthreads
void *calcular_pi(void *n);             // declaracion de las funciones necesarias para pi con pthreads

void *mult_pthreads(void *n);           // declaracion de las funciones 
void pth_mult_vctr_mtrx(int n);         // declaracion de las funciones 
void mult(int n);                       // declaracion de las funciones 

int * mult_resultado_pthreads = NULL;   // inicializacion de las vectores 
int * mult_resultado = NULL;            // inicializacion de las vectores 
int * vector = NULL;                    // inicializacion de las vectores 
int * matriz = NULL;                    // inicializacion de las vectores 

int step_i = 0;                         // offset para la multiplicacion matriz x vector

int main()
{ 
    srand(time(0));                                     // semilla para rand()
    int n = (rand() % (upper - lower + 1)) + lower;     // n aleatorio entre upper y lower
    mult_resultado_pthreads = malloc(n);                // reservamos memoria para los vectores
    mult_resultado = malloc(n);                         // reservamos memoria para los vectores
    vector = malloc(n);                                 // reservamos memoria para los vectores
    matriz = (int *)malloc(n*n*sizeof(int));            // reservamos memoria para los vectores

    double pi;                                          // variable para guardar la aprox. de pi
    FILE * fp;                                          // abrimos el archivo para guardar la aprox. de pi
    fp = fopen ("./results/resultados_pi.txt","w");

    pi = pi_aprox(NUM_TERMINOS_PI);                     // aproximamos pi
    
    fprintf(fp, "pi: %.15f", pi);                       // guardamos el resultado en el .txt
    fclose (fp);

    pthread_mutex_init(&mutex, NULL);                   // iniciamos la variable mutex
    pth_pi_aprox(NUM_TERMINOS_PI);                      // calculamos pi con pthreads
 
    int upper_rand = 100;                               // rango para los numeros aleatorios que van a llenar la matriz y el vector
    int lower_rand = 0;                                 // rango para los numeros aleatorios que van a llenar la matriz y el vector

    printf("\n\n\tvector: ");
    for (int i = 0; i < n; i++)                         // llenamos el vector
    {
        vector[i] =  (rand() % (upper_rand - lower_rand + 1)) + lower_rand;
        printf("\n\t\t[%d] = %d", i, vector[i]);
        mult_resultado_pthreads[i] = 0;
    }

    printf("\n\n\tmatriz: \n");                         // llenamos la matriz
    for (int i = 0; i < n; i++)
    {
        printf("\t\t");
        for (int j = 0; j < n; j++)
        {
            matriz[i*n+j] =  (rand() % (upper_rand - lower_rand + 1)) + lower_rand;
            printf("[%d][%d] = %d\t", i, j, matriz[i*n+j]);
        }
        printf("\n");
    }

    mult(n);                                            // hacemos la multiplicacion sin pthreads

    pthread_mutex_init(&mutex_mtrx, NULL);                   // iniciamos la variable mutex
    pth_mult_vctr_mtrx(n);                              // hacemos la multiplicacion con pthreads

    free(vector);                                       // liberamos memoria de los vectores 
    free(matriz);                                       // liberamos memoria de los vectores 
    free(mult_resultado_pthreads);                      // liberamos memoria de los vectores 
    free(mult_resultado_pthreads);                      // liberamos memoria de los vectores 

    pthread_exit(NULL);                                 // terminamos los pthreads
    return 0;    
}

/*
 * Aproxima Pi con la Serie de Leibniz hasta i = n utilizando pthreads
 * 
 * Pi = 4 Sum (-1)^n/(2n+1)
 * 
 * [in] n : limite superior de la sumatoria
 *
 * return double aproximacion de pi
 */
double pth_pi_aprox(int n)
{
    FILE * fp_pthreads;                                                 // abrimos el archivo para guardar 
    fp_pthreads = fopen ("./results/resultados_pi_pthreads.txt","w");   //    la aprox. de pi con pthreads
    
    void *status;                                                       // puntero para join
    
    printf("\n\tcreando pthreads: \n");
    for (int i = 0; i < NUM_THREADS; i++)
    {
        pthread_create(&threads[i],NULL, calcular_pi, &n);              // creamos los n threads
    }

    for ( int i = 0 ; i < NUM_THREADS; i++ ) 
    {  
        pthread_join(threads[i], &status);                              // los unimos
    }
    printf("\n\tresultado de join pthreads = %.15f\n", pthreads_resultado.aprox_pi_pthreads);

    fprintf(fp_pthreads, "pi: %.15f", pthreads_resultado.aprox_pi_pthreads);    // guardamos el resultado
    fclose (fp_pthreads);                                                       //      en el .txt

    printf("\n");
    pthread_mutex_destroy(&mutex);                          // destruimos el mutex
    //pthread_exit(NULL);                                   // terminamos los threads
    return pthreads_resultado.aprox_pi_pthreads;
}

/*
 * Realiza la sumatoria para pi con pthreads
 * 
 * [in] n : limite superior de la sumatoria
 *
 * return void
 */
void *calcular_pi(void *n)
{
    int iteraciones = *(int*)n;                     // numero de terminos
	double pi = 0.0;                                // variable para el resultado
    for(int i = 0; i <= iteraciones; i++)           // calcula pi con la sumatoria de leibniz
    {
        pi = pi + pow(-1.0, (double)i)*(1.0/((2.0*(double)i)+1.0)); 
    }
    pi = pi*4.0;                                    // la sumatoria de resultado pi/4, entonces multiplicamos por 4

    pthread_mutex_lock(&mutex);                     // le damos la llave mutex al thread
    
    printf("\t\tThread obtained mutex\n");      
    pthreads_resultado.aprox_pi_pthreads = pi;      // el escribe el resultado

    pthread_mutex_unlock(&mutex);                   // liberamos mutex
    return 0;
}

/*
 * Multiplica un vector nx1 y una matriz de nxn con pthreads
 * 
 * [in] n : parametro de tamano
 *
 */
void *mult_pthreads(void *n)
{
    int iteraciones = *(int*)n; 
    if(step_i>=iteraciones){return 0;}
    pthread_mutex_lock(&mutex_mtrx);                     // le damos la llave mutex al thread
    for (int k = 0; k < iteraciones; k++)
    {
        mult_resultado_pthreads[step_i] += matriz[step_i*iteraciones+k]*vector[k];  // cada thread hace 1/n de la matriz   
    }
    step_i++;
    pthread_mutex_unlock(&mutex_mtrx);                   // liberamos mutex
    return 0;
}

/*
 * Multiplica un vector nx1 y una matriz de nxn sin pthreads
 * 
 * [in] n : parametro de tamano
 *
 */
void mult(int n)
{
    FILE * fp_mult;                                              // abrimos el archivo para guardar la aprox. de pi
    fp_mult = fopen ("./results/resultados_vctr_mtrx.txt","w");

    int iteraciones = n; 
    for (int i = 0; i < iteraciones; i++)
    {
        for (int k = 0; k < iteraciones; k++)
        {
            mult_resultado[i] += matriz[i*n+k]*vector[k];       // multiplicamos la matriz y el vector
        }
    }
    printf("\n\tresultado de mult");
    for (int i = 0; i < n; i++)
    {
        printf("\n\t\t[%d] = %d", i, mult_resultado[i]);
        fprintf(fp_mult, "%d\n", mult_resultado[i]);             // guardamos el resultado en el .txt
    }
    fclose (fp_mult);
}

/*
 * Multiplica un vector nx1 y una matriz de nxn con pthreads
 * 
 * [in] n : parametro de tamano
 *
 */
void pth_mult_vctr_mtrx(int n)
{     
    void *status;                                                       // puntero para join
    printf("\n\n");

    //printf("\n\tcreando pthreads: \n");
    for (int i = 0; i < n; i++)
    {
        pthread_create(&threads_mult[i],NULL, mult_pthreads, &n);       // creamos los n threads
    }

    for ( int i = 0 ; i < n; i++ ) 
    {  
        pthread_join(threads_mult[i], &status);                         // los unimos
    }
    
    printf("\n\tresultado de mult con %d pthreads", n);
    FILE * fp_mult_pth;                                                 // abrimos el archivo para guardar la aprox. de pi
    fp_mult_pth = fopen ("./results/resultados_vctr_mtrx_pthreads.txt","w");

    for (int i = 0; i < n; i++)
    {
        printf("\n\t\t[%d] = %d", i, mult_resultado_pthreads[i]);   // imprime el resultado
        fprintf(fp_mult_pth, "%d\n", mult_resultado_pthreads[i]);   // guardamos el resultado en el .txt
    }
    fclose (fp_mult_pth);
    printf("\n");
    pthread_mutex_destroy(&mutex_mtrx);                          // destruimos el mutex
    pthread_exit(NULL);                                          // terminamos los threads
    
}