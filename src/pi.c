#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "../include/pi.h"

double pi_aprox(int n)
{
    double pi = 0.0;
    for(int i = 0; i <= n; i++)
    {
        pi = pi + pow(-1.0, (double)i)*(1.0/((2.0*(double)i)+1.0));
    }

    pi = pi*4.0;
    printf("\n\taproximacion de pi con %i terminos: %.15f\n\n", n, pi);

    return pi;
}
    