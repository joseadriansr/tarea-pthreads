#include <stdio.h>
#include <stdlib.h>
#include <time.h>    // time()
#include <math.h>
#include <stdio.h>
#include "../include/mult_vctr_mtrx.h"

#define upper 100
#define lower 1

void mult_vctr_mtrx(int n)
{
    int vector [n];
    int matriz [n][n];
    int resultado [n];

    srand(time(0)); 
    printf("\n\n\tvector: \n");
    for (int i = 0; i < n; i++)
    {
        vector[i] = (rand() % (upper - lower + 1)) + lower;
        printf("\n\t\t[%d] = %d", i, vector[i]);
        resultado[i] = 0;
    }

    printf("\n\n\tmatriz: \n");
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            matriz[i][j] = (rand() % (upper - lower + 1)) + lower;
            printf("\t\t[%d][%d] = %d", i, j, matriz[i][j]);
        }
        printf("\n");
    }

    FILE * fp;
    fp = fopen ("./results/resultados_vctr_mtrx.txt","w");
    
    printf("\n\n\tresultado: \n");
    for (int i = 0; i < n; i++)
    {
        for (int k = 0; k < n; k++)
        {
            resultado[i] += matriz[i][k]*vector[k];
        }
        printf("\n\t\t[%d] = %d", i, resultado[i]);
        fprintf(fp, "[%d] = %d\n", i, resultado[i]);
    }
    fclose (fp);

    printf("\n\n");
}
    