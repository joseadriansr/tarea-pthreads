#!/bin/bash

# Lo tomamos de la tarea de saltos

# This scripts does a simple comparison of two files
# using diff cmd

# Variable to check for errors
error=0

# Corremos el programa y comparamos los .txt de salida
echo "Corremos el programa..."
./bin/main 
a= cmp -s ./results/resultados_pi.txt ./results/resultados_pi_pthreads.txt
echo "Revisamos si hay diferencias en pi..."
if ((a == 0)); then
   echo "=====> Los resultados son iguales..."
else
   echo "=====> Los resultados son diferentes..."
   error=1
fi

b= cmp -s ./results/resultados_vctr_mtrx.txt ./results/resultados_vctr_mtrx_pthreads.txt
echo "Revisamos si hay diferencias en la multiplicacion de matriz x vector..."
if ((b == 0)); then
   echo "=====> Los resultados son iguales..."
else
   echo "=====> Los resultados son diferentes..."
   error=1
fi

#rm ./results/*.txt

if (($error == 1));then
  exit -1
fi
