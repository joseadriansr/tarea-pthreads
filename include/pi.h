
#ifndef PI_H
#define PI_H

/*
 * Aproxima Pi con la Serie de Leibniz hasta i = n
 * 
 * Pi = 2 Sum (-1)^n/(2n+1)
 * 
 * [in] n : limite superior de la sumatoria
 *
 * return double aproximacion de pi
 */
double pi_aprox(int n);

#endif