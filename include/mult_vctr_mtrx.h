
#ifndef MULT_VCTR_MTRX_H
#define MULT_VCTR_MTRX_H

/*
 * Multiplica un vector nx1 y una matriz de nxn
 * 
 * [in] n : parametro de tamano
 *
 */
void mult_vctr_mtrx(int n);

#endif